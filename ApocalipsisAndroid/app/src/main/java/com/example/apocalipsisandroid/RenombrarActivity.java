package com.example.apocalipsisandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class RenombrarActivity extends AppCompatActivity implements View.OnClickListener {
    EditText txtRenombre;
    Button btnAceptar, btnCancelar;
    SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_renombrar);
        txtRenombre=findViewById(R.id.txtRenombre);
        btnAceptar=findViewById(R.id.btnAceptar);
        btnCancelar=findViewById(R.id.btnCancelar);

        btnCancelar.setOnClickListener(this);
        btnAceptar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v==btnAceptar){
            db=MainActivity.dbh.getWritableDatabase();
            ContentValues cv = new ContentValues();

            cv.put("usuario",txtRenombre.getText().toString());
            db.update("Usuarios",cv,"usuarioId=?",new String[]{MainActivity.idActual});
        }
        Intent i = new Intent(getApplicationContext(),InicioActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(i);
    }
}
