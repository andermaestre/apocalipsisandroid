package com.example.apocalipsisandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

public class LeccionesActivity extends AppCompatActivity implements View.OnClickListener {

    TextView txtTitulo, txtContenido;
    ScrollView scroll;

    Cursor c;
    SQLiteDatabase db;

    Button btnAdelante, btnAtras;
    int indice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lecciones);

        indice=2;

        txtTitulo=findViewById(R.id.txtTitulo);
        txtContenido=findViewById(R.id.txtContenido);
        scroll=findViewById(R.id.scrollContenido);
        btnAdelante=findViewById(R.id.btnAdelante);
        btnAtras=findViewById(R.id.btnAtras);
        btnAtras.setOnClickListener(this);
        btnAdelante.setOnClickListener(this);

        db=MainActivity.dbh.getReadableDatabase();
        c=db.rawQuery("SELECT temaId, titulo, contenido FROM Temas WHERE temaId = ?",new String[]{Integer.toString(indice)});
        if(c.moveToFirst()){
            txtTitulo.setText(c.getString(1));
            txtContenido.setText(c.getString(2));

        }
        db.close();
    }

    @Override
    public void onClick(View v) {
        int max;
        db=MainActivity.dbh.getReadableDatabase();
        c = db.rawQuery("SELECT * FROM Temas",null);
        max=c.getCount();
        txtTitulo.setText("");
        txtContenido.setText("");
        if(v==btnAdelante&&indice<max){
            indice++;

        }else if(v==btnAtras&&indice>1){
            indice--;
        }
        c=db.rawQuery("SELECT temaId, titulo, contenido FROM Temas WHERE temaId = ?",new String[]{Integer.toString(indice)});
        if(c.moveToFirst()){
            txtTitulo.setText(c.getString(1));
            txtContenido.setText(c.getString(2));

        }
        db.close();
    }
}
