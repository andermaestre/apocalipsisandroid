package com.example.apocalipsisandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AltaUsuariosActivity extends AppCompatActivity implements View.OnClickListener{

    EditText txtUsuario, txtPass, txtNombre;
    Button btnSignin;

    SQLiteDatabase db;

    int newId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alta_usuarios2);

        txtUsuario=findViewById(R.id.txtUser2);
        txtPass=findViewById(R.id.txtPass2);
        txtNombre=findViewById(R.id.txtNombre2);

        btnSignin=findViewById(R.id.btnSignin);
        btnSignin.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        if(v==btnSignin){
            db =MainActivity.dbh.getWritableDatabase();
            Cursor c =db.rawQuery("SELECT MAX(usuarioId) FROM Usuarios",null);
            if(c.moveToFirst()){
                newId=c.getInt(0)+1;
            }else{
                newId=1;
            }


            ContentValues cv= new ContentValues();
            cv.put("usuarioId", newId);
            cv.put("usuario", txtUsuario.getText().toString());
            cv.put("pass", txtPass.getText().toString());
            cv.put("nombre",txtNombre.getText().toString());

            db.insert("Usuarios",null,cv);

            db.close();
            Intent i = new Intent(this,MainActivity.class);
            startActivity(i);
        }
    }
}
