package com.example.apocalipsisandroid;

public class Resultado {
    public int idResultado;
    public int nota;
    public String nombreTema;

    public Resultado(int idResultado, int nota, String nombreTema) {
        this.idResultado = idResultado;
        this.nota = nota;
        this.nombreTema=nombreTema;
    }

    public int getIdResultado() {
        return idResultado;
    }

    public void setIdResultado(int idResultado) {
        this.idResultado = idResultado;
    }

    public int getNota() {
        return nota;
    }

    public void setNota(int nota) {
        this.nota = nota;
    }

    public String getNombreTema() {
        return nombreTema;
    }

    public void setNombreTema(String nombreTema) {
        this.nombreTema = nombreTema;
    }
}
