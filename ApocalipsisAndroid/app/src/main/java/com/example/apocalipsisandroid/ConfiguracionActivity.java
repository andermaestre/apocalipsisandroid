package com.example.apocalipsisandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ApplicationErrorReport;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class ConfiguracionActivity extends AppCompatActivity implements View.OnClickListener {

    SQLiteDatabase db;
    Button btnEliminar, btnEliminarRes, btnRenombrar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracion);

        btnEliminar=findViewById(R.id.btnEliminar);
        btnEliminar.setOnClickListener(this);

        btnEliminarRes=findViewById(R.id.btnEliminarRes);
        btnEliminarRes.setOnClickListener(this);

        btnRenombrar=findViewById(R.id.btnRenombrar);
        btnRenombrar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v==btnEliminar)
        {
            eliminaUsuario();
        }else if(v==btnEliminarRes){
            eliminaResultados();
        }else if(v==btnRenombrar){
            Intent i = new Intent(this,RenombrarActivity.class);
            startActivity(i);
        }
    }

    private void eliminaResultados() {
        db=MainActivity.dbh.getWritableDatabase();
        db.delete("Resultados","usuarioId=?",new String[]{MainActivity.idActual});
        Toast.makeText(this,"Tus resultados son borrados",Toast.LENGTH_LONG);
    }

    private void eliminaUsuario() {
        db=MainActivity.dbh.getWritableDatabase();

        //Cursor c = db.query("Resultados",new String[]{"resultadoId" , "usuarioId" , "nota" , "IdTema"},"usuarioId=?",new String[]{MainActivity.idActual},null,null,null);

        db.delete("Resultados","usuarioId=?",new String[]{MainActivity.idActual});
        db.delete("Usuarios","usuarioId=?",new String[]{MainActivity.idActual});

        Toast.makeText(this,"Hasta la vista",Toast.LENGTH_LONG);
        Thread h =new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Intent i = new Intent(getApplicationContext(),MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(i);

            }
        });
        h.start();

    }
}
