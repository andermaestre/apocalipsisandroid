package com.example.apocalipsisandroid;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class ListViewAdaptador extends BaseAdapter {
    Context contexto;
    ArrayList<Resultado> lista;
    TextView txtId, txtTitulo, txtNota;

    public ListViewAdaptador(Context contexto, ArrayList<Resultado> lista) {
        this.contexto = contexto;
        this.lista = lista;

    }

    @Override
    public int getCount() {
        return this.lista.size();
    }

    @Override
    public Object getItem(int position) {
        return this.lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(this.contexto);
        v=layoutInflater.inflate(R.layout.item_lista,null);
        txtId=(TextView)v.findViewById(R.id.txtIdRes);
        txtTitulo=v.findViewById(R.id.txtTituloRes);
        txtNota=v.findViewById(R.id.txtNotaRes);
        txtId.setText(Integer.toString(lista.get(position).idResultado));
        txtTitulo.setText(lista.get(position).nombreTema);
        txtNota.setText(Integer.toString(lista.get(position).nota));


        return v;
    }
}
