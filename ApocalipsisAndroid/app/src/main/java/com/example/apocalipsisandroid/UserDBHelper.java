package com.example.apocalipsisandroid;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class UserDBHelper extends SQLiteOpenHelper {

    SQLiteDatabase db;

    public UserDBHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE Usuarios (usuarioId INTEGER PRIMARY KEY, usuario TEXT, pass TEXT, nombre TEXT)");
        db.execSQL("CREATE TABLE Temas (temaId INTEGER PRIMARY KEY, titulo TEXT, contenido TEXT)");
        db.execSQL("CREATE TABLE Resultados (resultadoId INTEGER PRIMARY KEY, usuarioId TEXT, nota INTEGER, IdTema INTEGER)");
        db.execSQL("CREATE TABLE Preguntas (preguntaId INTEGER PRIMARY KEY, pregunta TEXT, IdTema INTEGER, respuesta INTEGER, orden INTEGER)");
        db.execSQL("CREATE TABLE Respuestas (respuestaId INTEGER PRIMARY KEY, preguntaId INTEGER, respuesta TEXT, orden INTEGER)");

    }

    public int getIdResp(){
        int resp=1;

        db=MainActivity.dbh.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT MAX(resultadoId) FROM Resultados",null);
        if (c.moveToFirst()){
            resp=c.getInt(0)+1;
        }

        return resp;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
