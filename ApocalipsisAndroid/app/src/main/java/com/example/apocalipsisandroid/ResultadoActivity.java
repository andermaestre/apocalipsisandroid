package com.example.apocalipsisandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ResultadoActivity extends AppCompatActivity {

    SQLiteDatabase db;
    Cursor c;
    ListView lv;
    TextView txtNombre;
    public ArrayList<Resultado> resultados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultados);

        lv=findViewById(R.id.lv);

        db=MainActivity.dbh.getReadableDatabase();
        txtNombre=findViewById(R.id.txtNombre69);
        txtNombre.setText("Usuario: "+MainActivity.nombreUsuario);
        String IdAhora = MainActivity.idActual;
        Cursor c = db.rawQuery("SELECT r.resultadoId, r.nota, t.titulo FROM Resultados r JOIN Temas t ON r.IdTema=t.temaId WHERE usuarioId=?", new String[]{IdAhora});
        if(!c.moveToFirst()){
            Toast.makeText(this,"No tienes resultados", Toast.LENGTH_SHORT).show();
            return;
        }
        this.resultados = new ArrayList<>();
        do{
            crearCampoListView(c);
        }while(c.moveToNext());
        ListViewAdaptador lva = new ListViewAdaptador(this,  this.resultados);
        lv.setAdapter(lva);
    }

    private void crearCampoListView(Cursor c) {
        Resultado r = new Resultado(c.getInt(0), c.getInt(1), c.getString(2));
        this.resultados.add(r);
    }
}
