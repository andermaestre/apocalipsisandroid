package com.example.apocalipsisandroid;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

public class InicioActivity extends AppCompatActivity implements View.OnClickListener{

    Button btnLecciones, btnRetos, btnResultados, btnConfiguracion, btnMic;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        btnLecciones=findViewById(R.id.btnLec);
        btnRetos=findViewById(R.id.btnRetos);
        btnResultados=findViewById(R.id.btnResultados);
        btnConfiguracion=findViewById(R.id.btnConfiguracion);
        btnMic=findViewById(R.id.btnMic);

        btnLecciones.setOnClickListener(this);
        btnRetos.setOnClickListener(this);
        btnResultados.setOnClickListener(this);
        btnConfiguracion.setOnClickListener(this);
        btnMic.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode){
            case 10:
                if(resultCode == RESULT_OK && data!=null){
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    if(result.get(0).toLowerCase().compareTo("lecciones")==0){

                        Intent i = new Intent(getApplicationContext(),LeccionesActivity.class);
                        startActivity(i);
                    }else if(result.get(0).toLowerCase().compareTo("resultados")==0){

                        Intent i = new Intent(getApplicationContext(),ResultadoActivity.class);
                        startActivity(i);
                    }else if(result.get(0).toLowerCase().compareTo("retos")==0){

                        Intent i = new Intent(getApplicationContext(),TemasRetos.class);
                        startActivity(i);
                    }else if(result.get(0).toLowerCase().compareTo("configuracion")==0){

                        Intent i = new Intent(getApplicationContext(),ConfiguracionActivity.class);
                        startActivity(i);
                    }else{
                        Toast.makeText(this, "No te entiendo  "+result.get(0), Toast.LENGTH_SHORT).show();
                    }
                }


                break;
        }
    }

    @Override
    public void onClick(View v) {
        if(v==btnLecciones)
        {
            Intent i = new Intent(this,LeccionesActivity.class);
            startActivity(i);
        }else if (v==btnRetos)
        {
            Intent i = new Intent(this,TemasRetos.class);
            startActivity(i);
        }else if(v==btnResultados)
        {
            Intent i = new Intent(this,ResultadoActivity.class);
            startActivity(i);
        }else if (v==btnConfiguracion)
        {
            Intent i = new Intent(this,ConfiguracionActivity.class);
            startActivity(i);
        }else if (v==btnMic)
        {
            Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
            if(intent.resolveActivity(getPackageManager())!= null){
                startActivityForResult(intent,10);
            }else{
                Toast.makeText(this,"No tienes poder suficiente, farmea y vuelve",Toast.LENGTH_LONG).show();
            }
        }
    }
}
