package com.example.apocalipsisandroid;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    EditText txtUser, txtPass;
    Button btnLogin;
    static UserDBHelper dbh;
    SQLiteDatabase db;
    static String idActual;
    static String nombreUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        txtUser=findViewById(R.id.txtUser);
        txtPass=findViewById(R.id.txtPass);

        btnLogin=findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);

        dbh=new UserDBHelper(this,"BaseDatos.db",null,1);
    }

    @Override
    public void onClick(View v) {
        if(v==btnLogin)
        {
            db=dbh.getReadableDatabase();
            Cursor c =db.rawQuery("SELECT pass, usuarioId, nombre FROM Usuarios Where usuario=?",new String[] { txtUser.getText().toString()});


            if(c.moveToFirst()){
                //Existe el user
                if(c.getString(0).compareTo(txtPass.getText().toString())==0){
                    //Contraseña correcta
                    idActual = c.getString(1);
                    nombreUsuario=c.getString(2);
                    db.close();
                    Intent i = new Intent(this,InicioActivity.class);
                    startActivity(i);
                }else{
                    //Contraseña mal
                    Toast.makeText(this,"Mal la contraseña", Toast.LENGTH_LONG).show();
                }

            }else{
                //No existe el usuario
                Intent i = new Intent(this,AltaUsuariosActivity.class);
                startActivity(i);

            }

            db.close();

        }
    }
}
