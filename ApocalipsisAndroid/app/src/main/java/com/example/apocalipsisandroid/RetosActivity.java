package com.example.apocalipsisandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class RetosActivity extends AppCompatActivity implements View.OnClickListener {

    int resp, cont=1;
    TextView txtPregunta, txtMarcador;
    Button[] btnResp;
    SQLiteDatabase db;
    int marcador;
    int IdTema;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retos);
        marcador=0;
        IdTema=TemasRetos.IdTema;
        btnResp= new Button[4];
        txtPregunta=findViewById(R.id.txtPregunta);
        txtMarcador=findViewById(R.id.txtMarcador);
        txtMarcador.setText("Marcador: "+marcador);
        btnResp[0]=findViewById(R.id.resp1);
        btnResp[1]=findViewById(R.id.resp2);
        btnResp[2]=findViewById(R.id.resp3);
        btnResp[3]=findViewById(R.id.resp4);
        btnResp[1].setOnClickListener(this);
        btnResp[2].setOnClickListener(this);
        btnResp[3].setOnClickListener(this);
        btnResp[0].setOnClickListener(this);

        nuevaPregunta();


    }

    private void nuevaPregunta() {
        int i = 0;
        String raiz = "R.id.resp";
        db=MainActivity.dbh.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT pregunta, respuesta, preguntaId FROM Preguntas WHERE Orden=? AND IdTema=?", new String[]{Integer.toString(cont),Integer.toString(IdTema)});

        if (c.moveToFirst()) {
            resp = c.getInt(1);
            txtPregunta.setText(c.getString(0));
            int auxId=c.getInt(2);

            c = db.rawQuery("SELECT respuesta FROM Respuestas WHERE Orden=? AND preguntaId=?", new String[]{Integer.toString(cont),Integer.toString(auxId)});


            if (c.moveToFirst()) {
                do {
                    btnResp[i].setText(c.getString(0));

                    i++;
                } while (c.moveToNext());
            }
        }
        cont++;

    }

    @Override
    public void onClick(View v) {
        if(v==btnResp[1]){
            if(resp==2){
                //Acierta
                marcador++;
                txtMarcador.setText("Marcador: "+marcador);
            }else{
                //falla
            }

        }else if (v==btnResp[2]){
            if(resp==3){
                //Acierta
                marcador++;
                txtMarcador.setText("Marcador: "+marcador);
            }else{
                //falla
            }
        }else if (v==btnResp[3]){
            if(resp==4){
                //Acierta
                marcador++;
                txtMarcador.setText("Marcador: "+marcador);
            }else{
                //falla
            }
        }else if (v==btnResp[0]){
            if(resp==1){
                //Acierta
                marcador++;
                txtMarcador.setText("Marcador: "+marcador);
            }else{
                //falla

            }
        }
        if(cont<=10){
            nuevaPregunta();
        }else{
            Toast.makeText(this,"Terminado",Toast.LENGTH_LONG).show();
            int id = MainActivity.dbh.getIdResp();
            ContentValues cv = new ContentValues();
            cv.put("resultadoId",id);
            cv.put("usuarioId",MainActivity.idActual);
            cv.put("nota",marcador);
            cv.put("IdTema",IdTema);
            db.insert("Resultados",null,cv);
            Intent i = new Intent(getApplicationContext(),InicioActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(i);


        }
    }
}
