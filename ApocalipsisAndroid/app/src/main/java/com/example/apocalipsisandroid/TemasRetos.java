package com.example.apocalipsisandroid;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

public class TemasRetos extends AppCompatActivity implements View.OnClickListener {

    public static int IdTema;
    Button btnTema1, btnTema2,btnTema3, btnMicro;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temas_retos);
        btnTema1=findViewById(R.id.btnTema1);
        btnTema2=findViewById(R.id.btnTema2);
        btnTema3=findViewById(R.id.btnTema3);
        btnMicro=findViewById(R.id.btnMicro);
        btnTema1.setOnClickListener(this);
        btnTema2.setOnClickListener(this);
        btnTema3.setOnClickListener(this);
        btnMicro.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode){
            case 10:
                if(resultCode == RESULT_OK && data!=null){
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    if(result.get(0).toLowerCase().compareTo("tema 1")==0){
                        IdTema=1;
                        Intent i = new Intent(getApplicationContext(),RetosActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(i);
                    }else if(result.get(0).toLowerCase().compareTo("tema 2")==0){
                        IdTema=2;
                        Intent i = new Intent(getApplicationContext(),RetosActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(i);
                    }else if(result.get(0).toLowerCase().compareTo("tema 3")==0){
                        IdTema=3;
                        Intent i = new Intent(getApplicationContext(),RetosActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(i);
                    }else{
                        Toast.makeText(this, "No te entiendo  "+result.get(0), Toast.LENGTH_SHORT).show();
                    }
                }


                break;
        }
    }

    @Override
    public void onClick(View v) {
        if(v==btnTema1){
            IdTema=1;
            Intent i = new Intent(getApplicationContext(),RetosActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(i);
        }else if(v==btnTema2){
            IdTema=2;
            Intent i = new Intent(getApplicationContext(),RetosActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(i);
        }else if(v==btnTema3){
            IdTema=3;
            Intent i = new Intent(getApplicationContext(),RetosActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(i);
        }else if(v==btnMicro){
            Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
            if(intent.resolveActivity(getPackageManager())!= null){
                startActivityForResult(intent,10);
            }else{
                Toast.makeText(this,"No tienes poder suficiente, farmea y vuelve",Toast.LENGTH_LONG).show();
            }
        }
    }
}
